#!/usr/bin/env bash
set -e

FILE="jetbrains-toolbox-2.4.2.32922.tar.gz"
DIR="/opt/jetbrains-toolbox"
DEST=$PWD/$FILE

echo 'Downloading Jetbrains Toolbox...'
wget --quiet --continue --show-progress --progress=bar:force:noscroll https://download.jetbrains.com/toolbox/${FILE}
if sudo mkdir ${DIR}; then
    sudo tar -xzf "${DEST}" -C ${DIR} --strip-components=1
fi

echo 'Install Jetbrains Toolbox...'
sudo chmod -R +rwx ${DIR}
sudo touch ${DIR}/jetbrains-toolbox.sh

echo "$DIR/jetbrains-toolbox" | sudo tee -a $DIR/jetbrains-toolbox.sh > /dev/null
sudo ln -s ${DIR}/jetbrains-toolbox.sh /usr/local/bin/jetbrains-toolbox
sudo chmod -R +rwx /usr/local/bin/jetbrains-toolbox

sudo rm "${DEST}"
