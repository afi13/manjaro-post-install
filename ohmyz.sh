#!/usr/bin/env bash
set -e

sudo pacman -Sy zsh zsh-completions

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
