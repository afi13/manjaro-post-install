#!/usr/bin/env bash
set -e

sudo pacman -S docker
sudo pacman -S docker-compose 
sudo systemctl enable --now docker
sudo usermod -aG docker $USER