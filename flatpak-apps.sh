#!/usr/bin/env bash
set -e

flatpak install flathub com.slack.Slack
flatpak install flathub com.spotify.Client
flatpak install flathub us.zoom.Zoom