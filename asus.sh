#!/usr/bin/env bash
set -e

pacman-key --recv-keys 8F654886F17D497FEFE3DB448B15A6B0E9A3FA35
pacman-key --finger 8F654886F17D497FEFE3DB448B15A6B0E9A3FA35
pacman-key --lsign-key 8F654886F17D497FEFE3DB448B15A6B0E9A3FA35
pacman-key --finger 8F654886F17D497FEFE3DB448B15A6B0E9A3FA35

wget "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x8b15a6b0e9a3fa35" -O g14.sec
sudo pacman-key -a g14.sec

echo -e "\n[g14]\nServer = https://arch.asus-linux.org" | sudo tee -a /etc/pacman.conf
pacman -Suy

# Asusctl - custom fan profiles, anime, led control etc.
pacman -S asusctl
systemctl enable --now power-profiles-daemon.service

# Supergfxctl - graphics switching
pacman -S supergfxctl
systemctl enable --now supergfxd

# ROG Control Center
pacman -S rog-control-center
