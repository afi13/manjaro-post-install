#!/usr/bin/env bash
set -e # We want to fail at each command, to stop execution

echo 'Installing GIT...'
sudo pacman -Sy git

read -p "Enter your email: "  email
read -p "Enter your name: "  name

git config --global user.name $name
git config --global user.email $email

git config --global credential.helper store

# Git aliases
echo 'Creating GIT aliases...'
git config --global alias.nmerge "merge --no-ff"
git config --global alias.co "checkout"
git config --global alias.br "branch"
git config --global alias.ci "commit"
git config --global alias.st "status"
git config --global alias.unstage "reset HEAD --"
git config --global alias.last "log -1 HEAD"
git config --global alias.lg "log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative"
git config --global alias.uncommit "reset --soft HEAD^"
